import numpy as np
import cv2

cap = cv2.VideoCapture(0)
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
#out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))
#out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640,480))
cascPath = "/home/pi/opencv-3.2.0/data/haarcascades/haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
while(cap.isOpened()):
    ret, frame = cap.read()
    if ret == True:
#        frame = cv2.flip(frame,0)
    # write the flipped frame
        faces = faceCascade.detectMultiScale(
                                         frame,
                                         scaleFactor=1.1,
                                         minNeighbors=5,
                                         minSize=(30, 30)
                                         )
        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.imshow('frame',frame)
#        out.write(frame)
        key = cv2.waitKey(1)& 0xFF
        if key == ord('q'):
            break
    else:
        break
# Release everything if job is finish
cv2.destroyAllWindows()
cap.release()
#out.release()