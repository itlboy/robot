var usonic = require('mmm-usonic');
var wpi = require("wiring-pi");
wpi.wiringPiSetupSys();

var srf = function (ePin, trigPin, initCallback) {
    var echoPin = wpi.physPinToGpio(ePin);
    var triggerPin = wpi.physPinToGpio(trigPin);
    var _this = this;
    usonic.init(function (error) {
        if (error) {
        } else {
            _this.sensor = usonic.createSensor(echoPin, triggerPin, 1000);
        }
        
        if (initCallback) {
            initCallback(error);
        }
    });

}

srf.prototype = {
    getDistance: function () {
        return this.sensor();
    }
}

module.exports = srf;