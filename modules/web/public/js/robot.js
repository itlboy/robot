var Robot = function (url) {
    this.socket = io(url);
    this.socket.on('connect', function () {});
    this.socket.on('event', function (data) {});
    this.socket.on('disconnect', function () {});
    this.minSpeed = 0;
    this.maxSpeed = 100;
}
Robot.prototype = {
    run: function (method, variables) {
        var args = [];
        for (var key in arguments) {
            if (key != 0 && arguments.hasOwnProperty(key)) {
                args.push(arguments[key]);
            }
        }
        var data = {method: method, 'arguments': args};
        this.socket.emit("robot", data);
    },
    motor: function (method, variables) {
        var args = [];
        for (var key in arguments) {
            if (key != 0 && arguments.hasOwnProperty(key)) {
                args.push(arguments[key]);
            }
        }
        var data = {method: method, arguments: args};
        this.socket.emit("motor", data);
    }
}