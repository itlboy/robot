var express = require('express');
var path = require('path');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var cv = require('opencv');


app.use(express.static(path.resolve(__dirname, 'public')))
app.use(express.static("node_modules/nipplejs/dist"));

io.on('connection', function (socket) {
    socket.on('motor', function (data) {
        var method = data.method;
        var arguments = data.arguments;
        motor[method].apply(motor, arguments);
    });
    socket.on("disconnect", function () {
        motor.stop(function () {
            console.log("stop motor");
        })
    })
    motor.on("changeMotorSpeed", function (motor, speed) {
        socket.emit("changeMotorSpeed", {motor: motor, speed: speed});
        console.log("motor " + motor + ": " + speed);
    })
    motor.on("changeSpeed", function (speed) {
        socket.emit("changeSpeed", {speed: speed});
    })
});

//var camera = new cv.VideoCapture(0);
//setInterval(function () {
//    camera.read(function (err, im) {
//        if (err)
//            throw err;
//        im.detectObject(cv.FACE_CASCADE, {}, function (err, faces) {
//            if (faces) {
//                if (faces.length) {
//                    for (var i = 0; i < faces.length; i++) {
//                        var x = faces[i]
//                        im.ellipse(x.x + x.width / 2, x.y + x.height / 2, x.width / 2, x.height / 2);
//                    }
//                }
//                io.emit('newImage', {buffer: im.toBuffer()});
//            }
//        });
//    });
//}, 20);

server.listen(80)

