var events = require('events');
var wpi = require('wiring-pi');
var l298 = function (configs) {
    this.configs = configs;
    // Khai baó các cổng giao tiếp
    this.ins = configs.ins;
    this.motor = configs.motor;
    // Tốc độ hiện tại của hai motor
    this.currentSpeed = {1: 0, 2: 0};
    this.currentDirection = 0;
    // IntervalId cho sự thay đổi vận tốc hiện tại ứng với từng mô tơ
    this.currentChangeSpeedIntervals = {1: false, 2: false};
    this.setUp();
    this.events = {};
    this.events.prototype = new events.EventEmitter;
}


var prototype = {
    changeSpeedTo: function (speed, direction, acceleration, callback) {
        var finishCount = 0;
        var finish = function () {
            finishCount++;
            if (finishCount == 2) {
                if (callback) {
                    callback();
                }
            }
        }

        var motorsSpeed = this.getMotorsSpeedFromSpeedAndDirection(speed, direction);
        var acces = this.getAcceFromSpeeds(acceleration, motorsSpeed[0], motorsSpeed[1]);
        this.changeMotorSpeedto(1, acces[0], motorsSpeed[0], function () {
            finish();
        })
        this.changeMotorSpeedto(2, acces[1], motorsSpeed[1], function () {
            finish();
        })
        this.emit("changeSpeed", speed);
    },
    getChangeDirectionMaxDeltaSpeed: function () {
        return this.getConfig("changeDirectionMaxDeltaSpeed") || 30;
    },
    getAcceFromSpeeds: function (acceleration, motorSpeed1, motorSpeed2) {
        var motor1CurrentSpeed = this.getCurrentSpeed(1);
        var motor2CurrentSpeed = this.getCurrentSpeed(2);


        var deltaMotor1Speed = Math.abs(motor1CurrentSpeed - motorSpeed1);
        var deltaMotor2Speed = Math.abs(motor2CurrentSpeed - motorSpeed2);


        var defaultAcce = acceleration || this.motor.defaultAcce;

        if (deltaMotor1Speed * deltaMotor2Speed == 0) {
            return [defaultAcce, defaultAcce];
        }

        if (deltaMotor1Speed == deltaMotor2Speed) {
            return [defaultAcce, defaultAcce];
        }
        if (deltaMotor1Speed > deltaMotor2Speed) {
            return [defaultAcce, defaultAcce * deltaMotor2Speed / deltaMotor1Speed];
        }

        if (deltaMotor2Speed > deltaMotor1Speed) {
            return [defaultAcce * deltaMotor1Speed / deltaMotor2Speed, defaultAcce];
        }

    },
    getMotorsSpeedFromSpeedAndDirection: function (speed, direction) {
        var motor1Speed;
        var motor2Speed;
        var maxDetalSpeed = this.getChangeDirectionMaxDeltaSpeed() * speed / 100 / 90;

        if (direction <= 90) {
            motor1Speed = speed;
            deltaSpeed = (direction) * maxDetalSpeed;
            motor2Speed = speed - deltaSpeed;
        }

        if (direction > 90 && direction <= 180) {
            motor1Speed = -1 * speed;

            deltaSpeed = (180 - direction) * maxDetalSpeed;
            motor2Speed = motor1Speed + deltaSpeed;
        }

        if (direction > 180 && direction < 270) {
            motor2Speed = -1 * speed;

            deltaSpeed = (direction - 180) * maxDetalSpeed;
            motor1Speed = motor2Speed + deltaSpeed;
        }

        if (direction >= 270 && direction <= 360) {
            motor2Speed = speed;

            deltaSpeed = (360 - direction) * maxDetalSpeed;
            motor1Speed = motor2Speed - deltaSpeed;
        }
        return [motor1Speed, motor2Speed];
    },
    changeMotorSpeedto: function (motor, acceleration, speed, callback) {
        var currentSpeed = this.getCurrentSpeed(motor);
        if (currentSpeed == speed) {
            if (callback) {
                callback();
            }
            return;
        }

        var minSpeed = this.getConfig("minSpeed");

        console.log(minSpeed);

        if (Math.abs(speed) < minSpeed) {
            if (currentSpeed > 0) {
                if (speed >= currentSpeed) {
                    speed = minSpeed;
                } else {
                    speed = 0;
                }
            } else {
                if (speed <= currentSpeed) {
                    speed = minSpeed;
                } else {
                    speed = 0;
                }
            }
        }

        var deincrese = speed < this.getCurrentSpeed(motor);
        if (acceleration == undefined) {
            acceleration = this.motor.defaultAcce;
        }
        var l298 = this;
        this.changeMotorSpeed(motor, acceleration, deincrese, function (currentSpeed) {
            if (deincrese) {
                if (currentSpeed <= speed) {

                    if (currentSpeed < speed) {
                        l298.setMotorSpeed(motor, speed);
                    }

                    if (callback) {
                        callback();
                    }
                    return false;
                }
            } else {
                if (currentSpeed >= speed) {
                    if (currentSpeed > speed) {
                        l298.setMotorSpeed(motor, speed);
                    }
                    if (callback) {
                        callback();
                    }
                    return false;
                }
            }
            return true;
        })
    },
    /**
     * 
     * @param integer motor
     * @param float speed
     * @param float acceleration : {0,100], tỷ lệ % thay đổi với tốc độ tối đa tốc độ trong 1 giây
     * @returns bolean
     */

    changeMotorSpeed: function (motor, acceleration, deincrese, updateCallback, finishCallback) {
        var stepTime = this.motor[motor].increaseSpeedStep || 10;
        var stepSpeedRation = acceleration / 1000 * stepTime;
        var stepSpeed = (stepSpeedRation / 100) * this.getPwmRange();
        var l298 = this;

        this.removeSpeedInterval(motor);
        var run = function () {
            if (deincrese) {
                var newSpeed = l298.getCurrentSpeed(motor) - stepSpeed;
            } else {
                var newSpeed = l298.getCurrentSpeed(motor) + stepSpeed;
            }

//            if (Math.abs(newSpeed) < l298.getConfig('minSpeed')) {
//                if (deincrese) {
//                    console.log("hehe");
//                    newSpeed = -1 * l298.getConfig('minSpeed');
//                } else {
//                    console.log("h0h0");
//                    newSpeed = l298.getConfig('minSpeed');
//                }
//            }

            if (Math.abs(newSpeed) >= l298.getPwmRange()) {
                l298.removeSpeedInterval(motor);

                if (deincrese) {
                    newSpeed = -100;
                } else {
                    newSpeed = 100;
                }

                l298.setMotorSpeed(motor, newSpeed);

                if (finishCallback) {
                    finishCallback();
                }
                return;
            }

            l298.setMotorSpeed(motor, newSpeed);
            if (updateCallback) {
                var callbackResult = updateCallback(newSpeed);
                if (callbackResult === false) {
                    return  l298.removeSpeedInterval(motor);
                }
            }
        };
        var interval = setInterval(run, stepTime);
        run();

        this.setChangeSpeedInterval(motor, interval);
    },
    changeDirection: function (direction) {
        // Rẽ trái sẽ giảm vận tốc động cơ trái
        // Rẽ phải giảm vận tốc động cơ phải
        // Rẽ 90 độ sẽ giảm vận tốc về 0, rẽ x độ sẽ giảm vận tốc tỷ lệ (x/90)
        // góc âm là rẽ phải, dương là rẽ trái
        if (direction == 0) {
            return;
        }



        if (direction > 0) {
            var otherSide = 1;
            var changeSpeedSide = 2;
            // rẽ bên phải, giảm động cơ phải
        } else {
            var otherSide = 2;
            var changeSpeedSide = 1;
        }

        if (direction * this.getCurrentSpeed() < 0) {
            // Rẽ về hướng ngược lại với hướng đang rẽ
            // Sẽ thay đổi tốc độ cả hai động cơ
            // Động cơ bên không rẽ sẽ tăng tốc bằng động cơ rẽ hiện thời
            // Động cơ bên rẽ sẽ tính toán theo tỷ lệ với tốc độ mới
            var currentSpeedChangeSide = this.getCurrentSpeed(changeSpeedSide);
            var otherSideSpeed = currentSpeedChangeSide;
            this.setCurrentSpeed(otherSide, otherSideSpeed);
        } else {
            var otherSideSpeed = this.getCurrentSpeed(otherSide);
        }

        var changeSideSpeed = (direction / 90) * otherSideSpeed;
        this.setMotorSpeed(changeSpeedSide, changeSideSpeed);
        this.setCurrentDirection(direction);

    },
    setCurrentDirection: function (direction) {
        this.currentDirection = direction;

    },
    setChangeSpeedInterval: function (motor, inteval) {
        this.currentChangeSpeedIntervals[motor] = inteval;
    },
    removeSpeedInterval: function (motor) {
        clearInterval(this.currentChangeSpeedIntervals[motor]);
        this.setChangeSpeedInterval(motor, false);
    },
    setMotorSpeed: function (motor, speed) {
        var forward = true;
        var realSpeed = (Math.abs(speed) / 100) * this.getPwmRange();
        var stopIn;
        var runIn;
        if (speed < 0) {
            forward = false;
        }
        switch (motor) {
            case 1:
                if (forward) {
                    runIn = 1;
                    stopIn = 2;
                } else {
                    runIn = 2;
                    stopIn = 1;
                }
                break;
            case 2:
                if (forward) {
                    runIn = 3;
                    stopIn = 4;
                } else {
                    runIn = 4;
                    stopIn = 3;
                }
                break;
        }
        this.writePin(stopIn, 0);
        this.writePin(runIn, realSpeed);
        this.setCurrentSpeed(motor, speed);
    },
    setCurrentSpeed: function (motor, speed) {
        this.emit("changeMotorSpeed", motor, speed);
        return this.currentSpeed[motor] = speed;
    },
    getCurrentSpeed: function (motor) {
        return this.currentSpeed[motor];
    },
    setUp: function () {

        this.stopAcce = this.configs.stopAcce || 100;

        if (!this.isTestMode()) {
            var setupMode = this.configs.wpiSetupMode || 'phys';
            wpi.setup(setupMode);
            for (var key in this.ins) {
                if (!this.ins.hasOwnProperty(key))
                    continue;
                var pin = this.ins[key];
                var pwmRange = this.getPwmRange();
                wpi.pinMode(pin, wpi.OUTPUT);
                wpi.softPwmCreate(pin, 0, pwmRange);
            }
        }
    },
    getPwmRange: function () {
        return this.configs.pwmRange || 100;
    },
    writePin: function (input, value) {
        var pin = this.getWpipin(input);
        if (this.getConfig('testMode')) {
            return;
        }
        wpi.softPwmWrite(pin, parseInt(value));
    },
    getConfig: function (name) {
        return this.configs[name];
    },
    isTestMode: function () {
        return this.getConfig('testMode') || false;
    },
    reset: function () {
        var inputNumbers = [1, 2, 3, 4];
        for (var key in inputNumbers) {
            if (!inputNumbers.hasOwnProperty(key)) {
                continue;
            }
            var input = inputNumbers[key];
            this.writePin(input, 0);
        }
    },
    stop: function (callback) {
        this.removeSpeedInterval(1);
        this.removeSpeedInterval(2);
        var finishCount = 0;
        var finish = function () {
            if (finishCount === 1) {
                //finish
                if (callback) {
                    callback();
                }
            } else {
                finishCount++;
            }
        }
        this.changeSpeedTo(0, 0, this.stopAcce, callback);
    },
    getWpipin: function (input) {
        return this.ins[input];
    }
};

l298.prototype = Object.assign(new events.EventEmitter, prototype);
module.exports = l298;