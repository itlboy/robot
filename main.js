var l298 = require('./modules/l298');
var cluster = require("cluster");


global.Configs = require('./configs/main');



var configs = {
    ins: {
        1: 13,
        2: 11,
        3: 15,
        4: 16,
    },
    minSpeed: 0,
    testMode: false,
    changeDirectionMaxDeltaSpeed: 70,
    motor: {
        1: {
            increaseSpeedStep: 30
        },
        2: {
            increaseSpeedStep: 30
        },
        defaultAcce: 60,
    }
};

global.motor = new l298(configs);

process.on('uncaughtException', function (err) {
    // handle the error safely
    console.error(err)
})

process.on('SIGINT', function () {
    motor.stop(function () {
        console.log("on exit, stop motor");
        process.exit();
    })
})

//motor.on("changeSpeed",function(motor,speed){
//    console.log("motor "+motor+": "+speed);
//})

//motor.changeSpeedTo(50);

//if (cluster.isMaster) {
//    // Fork workers.
//    for (var i = 0; i < 4; i++) {
//        cluster.fork();
//    }
//} else {
var app = require("./modules/web/app");
//}


console.log("started ...");