var wpi = require('wiring-pi');
//var Promise = require('promise');
//var keypress = require('keypress');

wpi.setup('phys');

var maxRange = 100;
var in1 = 11;
var in2 = 13;
var in3 = 15;
var in4 = 16;

var defaultSpeed = 50;
var speed = 35;

wpi.pinMode(in1, wpi.OUTPUT);
wpi.pinMode(in2, wpi.OUTPUT);
wpi.pinMode(in3, wpi.OUTPUT);
wpi.pinMode(in4, wpi.OUTPUT);

wpi.digitalWrite(in1, 0);
wpi.digitalWrite(in2, 0);
wpi.digitalWrite(in3, 0);
wpi.digitalWrite(in4, 0);



//forward(100);
//
//wpi.delay(2000);

//bindKeys();

var firstKey = true;
var keyPress = false;
var keyPressTimeout = false;
function bindKeys() {
    keypress(process.stdin);

    process.stdin.on('keypress', function (ch, key) {
        keyCheck();
        if (key) {
            if (key.name == 'up') {
                forward(speed);
            }
            if (key.name == 'down') {
                back(speed);
            }
            if (key.name == 'left') {
                left(speed);
            }
            if (key.name == 'right') {
                right(speed);
            }
            if (key.name == 'return') {
                reset();
            }

            if (key.ctrl && key.name == 'c') {
                reset();
                process.stdin.pause();
            }
        }
    });

    process.stdin.on("keyup", function (ch, key) {
        console.log(key);
    })

    process.stdin.setRawMode(true);
    process.stdin.resume();
}

function keyCheck() {
    if (keyPress) {
        clearTimeout(keyPressTimeout);
        keyPressTimeout = setTimeout(function () {
            keyPress = false;
            keyup();
        }, 150)
    } else {
        keyPress = true;
        keyPressTimeout = setTimeout(function () {
            keyPress = false;
            keyup();
        }, 500)
    }
}
function keyup() {
    reset();
}

function test() {
    forward(1000);
    back(1000);
    left(1000);
    right(1000);
}

function reset() {
    wpi.softPwmWrite(in1, wpi.LOW);
    wpi.softPwmWrite(in2, wpi.LOW);
    wpi.softPwmWrite(in3, wpi.LOW);
    wpi.softPwmWrite(in4, wpi.LOW);
}

function left(speed, time) {
    reset();
    rightForward(speed, time);
    if (time) {
        wpi.delay(time);
        reset();
    }
}

function right(speed, time) {
    reset();
    leftForward(speed, time);
    if (time) {
        wpi.delay(time);
        reset();
    }
}

function forward(speed, time) {
    reset();
    leftForward(speed);
    rightForward(speed);
    if (time) {
        wpi.delay(time);
        reset();
    }
}

function back(speed, time) {
    reset();
    leftBack(speed);
    rightBack(speed);
    if (time) {
        wpi.delay(time);
        reset();
    }
}

function leftForward(speed, time) {
    return new Promise(function (success, error) {
        if (!speed) {
            speed = defaultSpeed;
        }

        wpi.softPwmWrite(in2, wpi.LOW);
        wpi.softPwmWrite(in1, speed);
        if (time) {
            setTimeout(function () {
                wpi.softPwmWrite(in1, wpi.LOW);
                if (success) {
                    success();
                }
            }, time)
        } else {
            if (success) {
                success();
            }
        }
    })
}

function leftBack(speed, time) {
    return new Promise(function (success, error) {
        if (!speed) {
            speed = defaultSpeed;
        }

        wpi.softPwmWrite(in1, wpi.LOW);
        wpi.softPwmWrite(in2, speed);
        if (time) {
            setTimeout(function () {
                wpi.softPwmWrite(in2, wpi.LOW);
                if (success) {
                    success();
                }
            }, time)
        } else {
            if (success) {
                success();
            }
        }
    })
}

function rightForward(speed, time) {
    return new Promise(function (success, error) {
        if (!speed) {
            speed = defaultSpeed;
        }

        wpi.softPwmWrite(in4, wpi.LOW);
        wpi.softPwmWrite(in3, speed);
        if (time) {
            setTimeout(function () {
                wpi.softPwmWrite(in3, wpi.LOW);
                if (success) {
                    success();
                }
            }, time)
        } else {
            if (success) {
                success();
            }
        }
    })
}

function rightBack(speed, time) {
    return new Promise(function (success, error) {
        if (!speed) {
            speed = defaultSpeed;
        }

        wpi.softPwmWrite(in3, wpi.LOW);
        wpi.softPwmWrite(in4, speed);
        if (time) {
            setTimeout(function () {
                wpi.softPwmWrite(in4, wpi.LOW);
                if (success) {
                    success();
                }
            }, time)
        } else {
            if (success) {
                success();
            }
        }
    })
}